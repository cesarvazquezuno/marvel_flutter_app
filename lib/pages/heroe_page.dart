import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:marvel_flutter_app/models/db.dart';
import 'package:marvel_flutter_app/models/personaje.dart';
import 'package:marvel_flutter_app/utils/constants.dart';
import 'package:marvel_flutter_app/widgets/list_heroe.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

/// Pantalla segundaria que muestra a los personajes por separado
class HeroePage extends StatefulWidget {
  const HeroePage ({ Key key, this.characterId }) : super(key: key);
  /// id del personaje a mostrar enviado desde la pantalla princiapl
  final int characterId;

  @override
  _HeroePageState createState() => _HeroePageState(myCharacterId: characterId);
}



class _HeroePageState extends State<HeroePage> {
  final int myCharacterId;
  _HeroePageState({this.myCharacterId});

  @override
  void initState() {
    super.initState();
    //fetchMarvelCharacter();
    //getPersonaje(myCharacterId);
  }

  Personaje personaje;

  /// Get de un solo personaje en SQLite
  Future<void> getPersonaje(int id) async {
    List<Personaje> auxPersonaj = await DB.personaje(id);

    setState(() {
      personaje = auxPersonaj[0];
    });
  }

  Map<String, dynamic> character = {};

  /// Get del personaje por id
  Future<void> fetchMarvelCharacter() async {
    String apiUrl ='https://gateway.marvel.com/v1/public/characters/$myCharacterId?ts=$timeStamp&apikey=$publicKey&hash=$hash';

    http.Response response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final jsonResponse = jsonDecode(response.body);
      setState(() {
        character = jsonResponse;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    //fetchMarvelCharacter();
    getPersonaje(myCharacterId);
    final ThemeData themeData = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: themeData.scaffoldBackgroundColor,
        shadowColor: Colors.white.withOpacity(0),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Card(
        child: personaje.id != null && personaje.id == myCharacterId
        ? Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListHeroe(
              image: '${personaje.path}.${personaje.extension}' ?? 'assets/images/spiderman.jpeg',
              name: personaje.name ?? 'Marvel Character',
              description: personaje.description ?? 'Description',
            )
          ],
        )
        : Center(child: CircularProgressIndicator()), /// Indicador loading
      ),
    );
  }
}
