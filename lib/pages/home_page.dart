import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:marvel_flutter_app/models/db.dart';
import 'package:marvel_flutter_app/models/personaje.dart';
import 'package:marvel_flutter_app/utils/constants.dart';
import 'package:marvel_flutter_app/widgets/custom_padding.dart';
import 'package:marvel_flutter_app/widgets/list_heroes.dart';
import '../utils/extensions.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/widgets.dart';

/// Pantalla principal que muestra el listado de personajes y filtro de búsqueda
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

/// Variables
bool filtrOn = false;
int intCharacters;
String stringCharacters;
String stringCharactersFiltrada;
List<dynamic> charactersFiltrada = [];

List<Personaje> personajes = [];

class _HomePageState extends State<HomePage> {
  /// Controlador de desplazamiento
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    /// Se elimina el controlador de desplazamiento cuando se elimine el widget
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels >= _scrollController.position.maxScrollExtent) {
      /// El usuario ha llegado al final de la lista
      int _offset = characters.length + 10;
      fetchMarvelCharacters(_offset);
    }
  }

  List<dynamic> characters = [];
  List<dynamic> charactersFiltrada = [];

  /// Get del listado de personajes MARVEL
  Future<void> fetchMarvelCharacters(int _offset) async {
  String apiUrl ='http://gateway.marvel.com/v1/public/characters?ts=$timeStamp&apikey=$publicKey&hash=$hash&limit=$_offset';

    http.Response response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      setState(() {
        characters = jsonResponse['data']['results'];
      });
    }
  }

  /// Insert a SQLite
  insertPersonajes() async {
    if (characters.isNotEmpty){

      final character = characters;

      for (var i = 0; i < character.length; i++) {
        DB.insert(Personaje(
          name: character[i]['name'],
          description: character[i]['description'],
          path: character[i]['thumbnail']['path'],
          extension:  character[i]['thumbnail']['extension']
        ));
      }
    }
  }

  /// Se limpia la tabla de personajes
  deletePersonajes() async {
    DB.deleteAll();
  }

  /// Get del listado de personajes en SQLite
  getPersonajes() async {
    List<Personaje> auxPersonaje = await DB.personajes();

    setState(() {
      personajes = auxPersonaje;
    });
  }

  @override
  Widget build(BuildContext context) {
    /// Primera carga del listado de personajes
    if (characters.length == 0){
      fetchMarvelCharacters(20);
    } else if(filtrOn == false){
      charactersFiltrada = personajes;
      intCharacters = personajes.length; 
      stringCharactersFiltrada = intCharacters.toString();

      deletePersonajes();
      insertPersonajes();
      getPersonajes();
    }

    if (charactersFiltrada.isEmpty){
      charactersFiltrada = personajes;
      intCharacters = personajes.length; 
    }

    stringCharacters = personajes.length.toString();

    /// Se agrega un listener para detectar cuándo se ha desplazado la lista
    _scrollController.addListener(_onScroll);

    return Scaffold(
      body: CustomPadding(
            child: Column(
            children: [
              kToolbarHeight.pv,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      const CircleAvatar(
                        backgroundColor: Colors.red,
                        child: Icon(
                          Icons.blur_circular,
                          color: Colors.white,
                        ),
                      ),
                      20.ph,
                      const Text("Bienvenido Sr. Stark")
                    ],
                  ),
                  SizedBox(
                    width: 100, /// Establece el ancho máximo del TextField
                    child: TextField(
                      onChanged: (String filtro){
                        setState(() {
                          filtrOn = true;
                          charactersFiltrada = personajes.where((elemento) => elemento.name.toLowerCase().contains(filtro.toLowerCase())).toList();
                          intCharacters = charactersFiltrada.length;
                          stringCharactersFiltrada = intCharacters.toString();
                        });
                      },
                      decoration: InputDecoration(
                        hintText: 'Busqueda...',
                      ),
                    ),
                  ),
                ],
              ),
              30.pv,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'PERSONAJES',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  Text(
                    '$stringCharactersFiltrada de $stringCharacters', /// Contador de personajes
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
              30.pv,
              Expanded(
                child: charactersFiltrada.isEmpty
                ? Center(child: CircularProgressIndicator()) /// Indicador loading
                : ListView.builder(
                  controller: _scrollController,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int index){
                    if (index >= 0 && index < charactersFiltrada.length) {
                      final character = charactersFiltrada[index];
                      return ListHeroes(
                          image: '${character.path}.${character.extension}',
                          name: character.name,
                          characterId: character.id,
                        );
                    }
                  },
                ))
            ],
          )
        ),
    );
  }
}
