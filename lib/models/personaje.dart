/// Clase para representar cada personaje
class Personaje {
  int id;
  String name;
  String description;
  String path;
  String extension;

  Personaje({this.id, this.name, this.description, this.path, this.extension});

  Map<String, dynamic> toMap() {
    return { 'id': id, 'name': name, 'description': description, 'path': path, 'extension': extension};
  }
}