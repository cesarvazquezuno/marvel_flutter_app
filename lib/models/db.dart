import 'personaje.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DB {
  static Future<Database> _openDB() async {
    return openDatabase(join(await getDatabasesPath(),'personajes.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE personajes (id INTEGER PRIMARY KEY, name TEXT, description TEXT, path TEXT, extension TEXT)",
        );
      }, version: 1);
  }

  static Future<void> insert(Personaje personaje) async {
    Database database = await _openDB();

    return database.insert("personajes", personaje.toMap());
  }

  static Future<void> delete(Personaje personaje) async {
    Database database = await _openDB();

    return database.delete("personajes",  where: "id = ?", whereArgs: [personaje.id]);
  }

  static Future<void> deleteAll() async {
    Database database = await _openDB();

    return database.delete("personajes");
  }

  static Future<void> update(Personaje personaje) async {
    Database database = await _openDB();

    return database.update("Personajes", personaje.toMap(), where: "id = ?", whereArgs: [personaje.id]);
  }

  static Future<List<Personaje>> personajes() async {
    Database database = await _openDB();
    final List<Map<String, dynamic>> personajesMap = await database.query("personajes");

    return List.generate(personajesMap.length,
      (i) => Personaje(
        id: personajesMap[i]['id'],
        name: personajesMap[i]['name'],
        description: personajesMap[i]['description'],
        path: personajesMap[i]['path'],
        extension: personajesMap[i]['extension'],
      ));
  }

  static Future<List<Personaje>> personaje(int id) async {
    Database database = await _openDB();
    final List<Map<String, dynamic>> resultado = await database.query('personajes', where: 'id = ?', whereArgs: [id]);

    return List.generate(resultado.length,
      (i) => Personaje(
        id: resultado[i]['id'],
        name: resultado[i]['name'],
        description: resultado[i]['description'],
        path: resultado[i]['path'],
        extension: resultado[i]['extension'],
      ));
  }
}