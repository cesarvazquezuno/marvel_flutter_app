import 'package:flutter/cupertino.dart';

/// Archivo reutilizable para definir padding horizontal y vertical
extension SizedBoxGeneratedByNum on num {
  Widget get ph => SizedBox( 
    width: this.toDouble(),
    );
  Widget get pv => SizedBox( 
    height: this.toDouble(),
    );
}