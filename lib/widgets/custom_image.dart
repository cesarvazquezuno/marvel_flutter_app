import 'package:flutter/material.dart';

/// Archivo reutilizable para agregar bordes redondeados a las imagenes
class CustomImage extends StatelessWidget{
  const CustomImage ({ Key key, this.child }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
            borderRadius: BorderRadius.circular(10), child: child);
  }
  
}