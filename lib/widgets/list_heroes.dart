import 'package:flutter/material.dart';
import 'package:marvel_flutter_app/pages/heroe_page.dart';
import '../utils/extensions.dart';
import 'custom_image.dart';

/// Archivo que define las características de cada elemento que se mostrara en la lista de personajes
class ListHeroes extends StatelessWidget{
  const ListHeroes ({ Key key, this.image, this.name, this.characterId }) : super(key: key);

  final String image;
  final String name;
  final int characterId;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      /// Gesto onTap para navegar la pantalla secundaria que muestra el detalle de cada personaje
      onTap: () => Navigator.push(context,
        MaterialPageRoute(builder: (context) => HeroePage(characterId: characterId))),    
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 100, child: CustomImage(
            child: Image.network(image)),
          ),
          5.pv,
          Text(
            name.toUpperCase(),
            style: TextStyle(fontWeight: FontWeight.w300),
          ),
          5.pv,
        ],
      )
    );
  }
  
}