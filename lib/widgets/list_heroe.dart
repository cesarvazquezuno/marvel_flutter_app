import 'package:flutter/material.dart';
import '../utils/extensions.dart';
import 'custom_image.dart';
import 'custom_padding.dart';

/// Archivo que define las características de cada personaje por separado
class ListHeroe extends StatelessWidget{
  const ListHeroe ({ Key key, this.image, this.name, this.description }) : super(key: key);

  final String image;
  final String name;
  final String description;

  @override
  Widget build(BuildContext context) {
    return GestureDetector( 
      child: CustomPadding(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(width: 250, child: CustomImage(child: Image.network(image))),
              15.pv,
              Text(name, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              15.pv,
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      /// Texto por default en caso de que el personaje no contenga una descripcón
                      text: description.isEmpty ? "A superhero is a fictional character whose characteristics exceed those of the classic hero, generally with superhuman powers but not necessarily, and linked to science fiction. Generated at the end of the year 1936 in the American comic book industry, which helped to raise it, they have enjoyed a multitude of adaptations to other media, especially in the cinema." : description,
                      style: TextStyle(color: Colors.grey)
                    )
                  ]
              )),
            ],
          ),
        ),
    );
  }
}