import 'package:flutter/material.dart';

/// Archivo reutilizable para agregar padding a las pantallas
class CustomPadding extends StatelessWidget {
  const CustomPadding({ Key key, this.child }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: child,
      );
  }
}