import 'package:flutter/material.dart';
import 'package:marvel_flutter_app/pages/home_page.dart';

/// Archivo main principal encargado de levantar la aplicación
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Marvel Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      /// La app iniciara en la HomePage que contiene el listado de personajes y el filtro
      home: HomePage(),
    );
  }
}
